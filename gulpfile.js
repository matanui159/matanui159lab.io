const gulp = require('gulp');
const clean = require('gulp-clean');
const parcel = require('gulp-parcel');
const purgecss = require('gulp-purgecss');
const gzip = require('gulp-gzip');
const serve = require('gulp-serve');
const merge = require('merge-stream');

function task_clean() {
   return gulp.src(['parcel', 'dest'], {allowEmpty: true})
      .pipe(clean());
}

function task_parcel(watch) {
   return function() {
      return gulp.src('src/*.html', {read: false})
         .pipe(parcel({
            outDir: 'parcel',
            publicUrl: '.',
            cache: false,
            watch: watch,
            sourceMaps: watch
         }));
   }
}

function task_compress() {
   return merge(
      gulp.src(['parcel/*.css'])
         .pipe(purgecss({
            content: ['parcel/*.html']
         })),
      gulp.src(['parcel/*', '!parcel/*.css'])
   )
      .pipe(gulp.dest('dest'))
      .pipe(gzip())
      .pipe(gulp.dest('dest'));
}

exports.clean = task_clean;
exports.build = gulp.series(task_clean, task_parcel(false), task_compress);
exports.watch = gulp.series(task_clean, task_parcel(true));
exports.serve = gulp.series(exports.watch, serve('parcel'));
exports.build_serve = gulp.series(exports.build, serve('dest'));