import {load as loadWebFonts} from 'webfontloader';

function loadCallback() {
   if (document.readyState === 'complete') {
      document.body.style.opacity = '1';
   } else {
      window.addEventListener('load', loadCallback);
   }
}

loadWebFonts({ 
   google: {
      families: ['Lobster', 'Raleway']
   },
   active: loadCallback,
   inactive: loadCallback,
   fontinactive: font => console.error(`Failed to load font: ${font}`)
});